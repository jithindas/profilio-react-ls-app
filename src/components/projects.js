import React, { Component } from "react";
import {
  Tab,
  Tabs,
  Card,
  CardTitle,
  CardText,
  CardActions,
  Button,
  CardMenu,
  IconButton,
  Grid,
  Cell,
} from "react-mdl";

class project extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0 };
  }

  toggleCategories() {
    if (this.state.activeTab === 0) {
      return (
        <div className="project-grid">
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://img-a.udemycdn.com/course/750x422/1362070_b9a1_2.jpg) center / cover",
              }}
            >
              React profilio
            </CardTitle>
            <CardText>
              About React is the online tutorial website. You can find the
              Example code related to React Native. We are trying our best to
              provide you the quality content.
            </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>GitLab</Button>
              <Button colored>youtube</Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>

          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://img-a.udemycdn.com/course/750x422/1362070_b9a1_2.jpg) center / cover",
              }}
            >
              React Cart
            </CardTitle>
            <CardText>
              About React is the online tutorial website. You can find the
              Example code related to React Native. We are trying our best to
              provide you the quality content.
            </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>GitLab</Button>
              <Button colored>youtube</Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://img-a.udemycdn.com/course/750x422/1362070_b9a1_2.jpg) center / cover",
              }}
            >
              React App{" "}
            </CardTitle>
            <CardText>
              About React is the online tutorial website. You can find the
              Example code related to React Native. We are trying our best to
              provide you the quality content.
            </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>GitLab</Button>
              <Button colored>youtube</Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
          <Card shadow={5} style={{ minWidth: "450", margin: "auto" }}>
            <CardTitle
              style={{
                color: "#fff",
                height: "176px",
                background:
                  "url(https://img-a.udemycdn.com/course/750x422/1362070_b9a1_2.jpg) center / cover",
              }}
            >
              React project
            </CardTitle>
            <CardText>
              About React is the online tutorial website. You can find the
              Example code related to React Native. We are trying our best to
              provide you the quality content.
            </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>GitLab</Button>
              <Button colored>youtube</Button>
            </CardActions>
            <CardMenu style={{ color: "#fff" }}>
              <IconButton name="share" />
            </CardMenu>
          </Card>
        </div>
      );
    } else if (this.state.activeTab === 1) {
      return (
        <div className="pythonn">
          <h1>This is Python</h1>
        </div>
      );
    } else if (this.state.activeTab === 2) {
      return (
        <div>
          <h1>This is Angular </h1>
        </div>
      );
    } else if (this.state.activeTab === 3) {
      return (
        <div>
          <h1>This is React Native </h1>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="catogary tabs">
        <Tabs
          activeTab={this.activeTab}
          onChange={(tabId) => this.setState({ activeTab: tabId })}
          ripple
        >
          <Tab>
            <b>React</b>
          </Tab>
          <Tab>
            <b>Python</b>
          </Tab>
          <Tab>
            <b>Angular</b>
          </Tab>
          <Tab>
            <b>React Native</b>
          </Tab>
        </Tabs>

        <Grid>
          <Cell col={12}>
            <div className="content">{this.toggleCategories()}</div>
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default project;
