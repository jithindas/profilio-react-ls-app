import React, { Component } from 'react'
import { Grid, Cell, List, ListItemContent, ListItem } from 'react-mdl'


class contact extends Component {
    render() {
        return (
            <div className="contact-body">
                <Grid className="contact-grid">
                    <Cell col={6}>
                        <h2>jithin das </h2>
                        <img
                        src="https://image.shutterstock.com/image-vector/businessman-icon-mens-avatar-260nw-383196565.jpg"
                        alt="avathar"
                        style={{height: '250px'}} />
                        <p style={{width: '75%', margin:'auto', paddingTop:'len'}}>
                        Gypsum is a soft sulfate mineral composed of calcium sulfate dihydrate, with the chemical formula CaSO4·2H2O. It is widely mined and is used as a fertilizer and as the main constituent in many forms of plaster, blackboard/sidewalk chalk, and drywall.
                        </p>
                    </Cell>
                    <Cell col={6}>
                        <h2>Contact me</h2>
                        <hr/>
                        <div className="contact-list">
                        <List>
                          <ListItem className="l1">
                            <ListItemContent style={{fontSize: '25px', fontFamily: 'Anton'}}>
                            <i class="fa fa-phone" aria-hidden="true"></i>  9747802290
                                </ListItemContent>
                         </ListItem>
                         <ListItem className="l1">
                            <ListItemContent style={{fontSize: '25px', fontFamily: 'Anton'}}>
                            <i class="fa fa-envelope" aria-hidden="true"></i>  9747802290
                                </ListItemContent>
                         </ListItem>
                         <ListItem className="l1">
                            <ListItemContent style={{fontSize: '25px', fontFamily: 'Anton'}}>
                            <i class="fa fa-skype" aria-hidden="true"></i>  examole@skype.com
                                </ListItemContent>
                         </ListItem>
                         {/* <ListItem>
                        <ListItemContent icon="person">JITHIN DAS P</ListItemContent>
                         </ListItem>
                         <ListItem>
                        <ListItemContent icon="person">Bob Odenkirk</ListItemContent>
                          </ListItem> */}
                    </List>
                    </div>
                    
                    </Cell>
                </Grid>
               
            </div>
            
        )
    }
}

export default contact
