import React, { Component } from 'react'
import { Grid, Cell, DataTable, TableHeader, Card, CardTitle, CardActions, Button, Icon ,FooterSection, FooterLinkList, Footer } from 'react-mdl'


class resume extends Component {
    render() {
        return (
            <div>
                <Grid className="demo-grid-1">
        <Cell col={4}>
            <div>
            <DataTable className="g1"
    selectable
    shadow={0}
    rowKeyColumn="id"
    rows={[
        {id: 1001, material: 'Acrylic (Transparent)', quantity: 25, price: 2.90},
        {id: 1002, material: 'Plywood (Birch)', quantity: 50, price: 1.25},
        {id: 1003, material: 'Laminate (Gold on Blue)', quantity: 10, price: 2.35}
    ]}
>
    <TableHeader name="material" tooltip="The amazing material name">Material</TableHeader>
    <TableHeader numeric name="quantity" tooltip="Number of materials">Quantity</TableHeader>
    <TableHeader numeric name="price" cellFormatter={(price) => `\$${price.toFixed(2)}`} tooltip="Price pet unit">Price</TableHeader>
</DataTable>
            </div>
        </Cell>
        <Cell col={4}>
            <div>
            <Card className="g2" shadow={0} style={{width: '256px', height: '256px', background: '#3E4EB8'}}>
    <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
        <h4 style={{marginTop: '0'}}>
            Featured event:<br />
            May 24, 2016<br />
            7-11pm
        </h4>
    </CardTitle>
    <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
        <Button colored style={{color: '#fff'}}>Add to Calendar</Button>
        <div className="mdl-layout-spacer"></div>
        <Icon name="event" />
    </CardActions>
</Card>
            </div>
        </Cell>
        <Cell col={4}>4</Cell>
    </Grid>

    <div>
    <Footer size="mini">
    <FooterSection type="left" logo="Title">
        <FooterLinkList>
            <a href="#">Help</a>
            <a href="#">Privacy & Terms</a>
        </FooterLinkList>
    </FooterSection>
</Footer>
    </div>
                
            </div>
            
        )
    }
}

export default resume
