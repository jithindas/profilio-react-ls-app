import React from "react";
import ladingpage from "./landingpage";
import aboutme from "./aboutme";
import resume from "./resume";
import projects from "./projects";
import contact from "./contact";

import { Switch, Route } from "react-router-dom";

const Main = () => (
  <Switch>
    <Route exact path="/" component={ladingpage} />
    <Route path="/landingpage" component={ladingpage} />
    <Route path="/aboutme" component={aboutme} />
    {/* <Route exact path="/resume"  component={resume} /> */}
    <Route exact path="/projects" component={projects} />
    <Route exact path="/contact" component={contact} />
  </Switch>
);

export default Main;
