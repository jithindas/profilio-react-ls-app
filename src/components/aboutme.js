import React, { Component } from 'react'
import {Grid, Cell, Footer, FooterLinkList, FooterSection, Spinner} from 'react-mdl'


class about extends Component {
    render() {
        return (
            <div className="abt">
                <Spinner />

                <Grid  className="dg3">
        <Cell  className="cl1" col={6} tablet={8}><h1>JITHIN DAS P</h1>
<h2>Panikker Thodi(House)<br/>Moorkkanad(P.O)<br/>Malappuram(Dist)<br></br>Kerala,India<br/>PIN-679338</h2>
<h5> +91 9995194223 |  jithindas454@gmail.com | <br/> gitlab.com/jithindas |  jithin-das-90060a18a</h5></Cell>
        <Cell  className="cl2" col={4} tablet={6}><h3>
        Languages</h3>
<h4> REACT JS, ANGULAR, JVASCRIPT, TYPESCRIPT, PYTHON, C SHARP, JAVA, C, C++, BASICS OF ANDROID, JAVASCRIPT, CSS,
HTML, PHP, SQL </h4>
<h3>Frameworks,IDE and Text-editors</h3>
<h4>DJANGO , PYCHARM, ANDROID STUDIO, VS CODE</h4>
<h3>OS and Others </h3>
<h4>WINDOWS, MAC, LINUX</h4></Cell>
        <Cell className="cl3" col={2} phone={4}><h6>
        MCA from APJ ABDULKALAM TECHNOLOGICAL UNIVERSITY(ongoing) Kerala, India
MES COLLEGE OF ENGINEERING KUTTIPPURAM 2018-present
<br/>
• Secured CGPA of 7.41 out of 10(till s4)
<br/>
<br/>
BCA from CALICUT UNIVERSITY Kerala, India
SAFA ARTS AND SCIENCE COLLEGE,POOKKATIRI 2014 - 2017 <br/>
• Secured 60.56 Percent
<br/>
<br/>
XII COMPUTER SCIENCE from KERALA STATE EDUCATION BOARD Kerala, India
N.H.S.S KOLATHUR 2012 - 2014
• Secured 70.76.33 Percent
<br/>
<br/>
X from KERALA STATE EDUCATION BOARD Kerala, India
M.E.S.H.S.S IRIMBILIYAM 2011 - 2012
• Secured 71.00 Percent</h6></Cell>
<br/>
    </Grid>
    <div>
    <Footer size="mini">
    <FooterSection type="left" logo="Be-Happy">
        <FooterLinkList>
            <a href="#">Help</a>
            <a href="#">Privacy & Terms</a>
        </FooterLinkList>
    </FooterSection>
</Footer>
    </div>

            </div>
            
            
            
        )
    }
}

export default about
